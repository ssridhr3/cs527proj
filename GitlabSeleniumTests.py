import os
from selenium import webdriver
from selenium.webdriver.firefox.firefox_binary import FirefoxBinary
import unittest
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common import action_chains
import time
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
import selenium
import subprocess

passw = os.environ['UIUC_P']
uname = os.environ['UIUC_U']

class SeleniumTests(unittest.TestCase):
	def signIn(self, browser, link):
		browser.get(link)
		username_bar = browser.find_element_by_xpath('//input[@id="username"]')
		username_bar.clear()
		username_bar.send_keys(uname)
		
		search_bar = browser.find_element_by_xpath('//input[@id="password"]')
		search_bar.send_keys(passw)
		search_bar.send_keys(Keys.RETURN)

	#Test 1

	def test_01_signIn(self):
		browser = webdriver.Firefox()
		link = 'https://gitlab.engr.illinois.edu'
		
		self.signIn(browser, link)
		wait = WebDriverWait(browser, 3)

		try:
			#if it signed in then it will show the projects page 
			new_proj_button = wait.until(EC.presence_of_element_located((By.XPATH, '//a[contains(text(), "Your projects")]')))
			self.assertTrue(True, "Logged in successfully")

		except selenium.common.exceptions.TimeoutException:
			self.assertFalse(True, "Unable to login")

		browser.quit()


	def test_02_createNewRepo(self):
		browser = webdriver.Firefox()

		self.signIn(browser, 'https://gitlab.engr.illinois.edu')

		wait = WebDriverWait(browser, 5)

		#waiting for new project button to appear
		new_proj_button = wait.until(EC.presence_of_element_located((By.XPATH, '//*[@class="btn btn-new"]')))

		new_proj_button.click()

		#setting url for the project
		project_path = wait.until(EC.presence_of_element_located((By.XPATH, '//input[@id="project_path"]')))
		project_path.send_keys("TestProjectGUI")

		#locating and clicking submit button
		submit = wait.until(EC.presence_of_element_located((By.XPATH, '//input[@class="btn btn-create project-submit"]')))
		submit.click()

		#if the repo was created it will show a message on the next page --> waiting for this message to appear

		try:
			page = wait.until(EC.presence_of_element_located((By.XPATH, '//span[contains(text(), "successfully created")]')))
			self.assertTrue(True, "Repo created successfully")
		
		except selenium.common.exceptions.TimeoutException:
			self.assertFalse(True, "Failed to create repo")

		browser.quit()

	def test_03_addReadmeFile(self):
		browser = webdriver.Firefox()

		self.signIn(browser, 'https://gitlab.engr.illinois.edu/' + uname + '/TestProjectGUI')

		wait = WebDriverWait(browser, 5)

		#waiting for 'readme' button to appear on page
		branches = wait.until(EC.presence_of_element_located((By.XPATH, '//a[contains(text(), "README")]')))
		branches.click()

		#locating and clicking on submit button
		submit_button = wait.until(EC.presence_of_element_located((By.XPATH, '//button[@class="btn commit-btn js-commit-button btn-create"]')))
		submit_button.click()

		time.sleep(2)

		#going back to repository page and checking if file shows up on page
		browser.get('https://gitlab.engr.illinois.edu/' + uname + '/TestProjectGUI')

		try:
			page = wait.until(EC.presence_of_element_located((By.XPATH, '//span[contains(text(), "README.md")]')))
			self.assertTrue(True, "README.md was successfully added")

		except selenium.common.exceptions.TimeoutException:
			self.assertFalse(True, "README.md was not successfully added")

		browser.quit()

	def test_04_AddFileToRepo(self):
		browser = webdriver.Firefox()
		self.signIn(browser, 'https://gitlab.engr.illinois.edu/' + uname + '/TestProjectGUI')

		wait = WebDriverWait(browser, 5)
		
		#waiting for the add file button to show up on page, then clicking it when it does
		plus_button = wait.until(EC.visibility_of_element_located((By.XPATH, '//a[@class="btn dropdown-toggle has-tooltip"]')))
		plus_button.click()
		
		#doing same with new file button
		new_file = wait.until(EC.visibility_of_element_located((By.XPATH, '//a[contains(text(), "New file")]')))
		new_file.click()
		
		#locating the textbox to enter file name
		filename = wait.until(EC.visibility_of_element_located((By.ID, 'file_name')))
		filename.send_keys('testfile.txt')

		content = browser.find_element_by_xpath('//textarea[@class="ace_text-input"]')
		content.send_keys("this is part of a GUI test")
		
		submit = wait.until(EC.element_to_be_clickable((By.XPATH, '//button[contains(text(), "Commit changes")]')))
		submit.click()
		
		#waiting for a few seconds for request to process
		time.sleep(2)

		try:
			browser.get('https://gitlab.engr.illinois.edu/' + uname + '/TestProjectGUI')

			#if filename shows up on page it means file was added successfully 
			filename = wait.until(EC.presence_of_element_located((By.XPATH, '//span[contains(text(), "testfile.txt")]')))
			self.assertTrue(True, "Added file successfully")
		
		except selenium.common.exceptions.TimeoutException:
			self.assertFalse(True, "Failed to add file")

		browser.quit()

	def test_05_delFile(self):
		browser = webdriver.Firefox()
		self.signIn(browser, 'https://gitlab.engr.illinois.edu/' + uname + '/TestProjectGUI')
		
		wait = WebDriverWait(browser, 5)

		filename = wait.until(EC.presence_of_element_located((By.XPATH, '//span[contains(text(), "testfile.txt")]')))
		filename.click()
		delete_button = wait.until(EC.presence_of_element_located((By.XPATH, '//button[contains(text(), "Delete")]')))
		delete_button.click()
		delete_file_button = wait.until(EC.presence_of_element_located((By.XPATH, '//button[contains(text(), "Delete file")]')))
		delete_file_button.click()

		try:
			filename = wait.until(EC.presence_of_element_located((By.XPATH, '//span[contains(text(), "testfile.txt")]')))
			self.assertFalse(True, "Failed to delete file")

		except selenium.common.exceptions.TimeoutException:
			self.assertTrue(True, "Deleted file successfully")

		browser.quit()

	def test_06_createNewBranch(self):
		browser = webdriver.Firefox()

		self.signIn(browser, 'https://gitlab.engr.illinois.edu/' + uname + '/TestProjectGUI')

		wait = WebDriverWait(browser, 5)

		repo_button = wait.until(EC.presence_of_element_located((By.XPATH, '//span[contains(text(), "Repository")]')))
		repo_button.click()

		time.sleep(1)

		branches = browser.find_element_by_xpath('//a[contains(text(), "Branches")]')
		branches.click()

		new_branches_button = wait.until(EC.presence_of_element_located((By.XPATH, '//a[contains(text(), "New branch")]')))
		new_branches_button.click()

		branch_name = wait.until(EC.presence_of_element_located((By.ID, 'branch_name')))
		branch_name.send_keys('branch-1')

		create_branch_button = browser.find_element_by_xpath('//button[@class="btn btn-create"]')
		create_branch_button.click()

		time.sleep(1)

		browser.get('https://gitlab.engr.illinois.edu/' + uname + '/TestProjectGUI/branches')
		branch = wait.until(EC.presence_of_element_located((By.XPATH, '//a[contains(text(), "branch-1")]')))

		try:
			self.assertTrue(True, "branch-1 was successfully created")

		except selenium.common.exceptions.TimeoutException:
			self.assertFalse(True, "branch-1 was not successfully created")

		browser.quit()

	def test_07_createMergeRequests(self):
		browser = webdriver.Firefox()
		self.signIn(browser, 'https://gitlab.engr.illinois.edu/' + uname + '/TestProjectGUI')

		wait = WebDriverWait(browser, 8)
		
		filename = wait.until(EC.presence_of_element_located((By.XPATH, '//span[contains(text(), "Merge Requests")]')))
		filename.click()

		merge_request_link = wait.until(EC.presence_of_element_located((By.ID, 'new_merge_request_link')))
		merge_request_link.click()

		dropdown = wait.until(EC.presence_of_element_located((By.XPATH, '//button[@class="dropdown-menu-toggle js-compare-dropdown js-source-branch git-revision-dropdown-toggle"]'))) 
		dropdown.click()

		branch_to_merge = browser.find_element_by_xpath('//*[@title="branch-1"]')
		branch_to_merge.click()

		compare_branches = browser.find_element_by_xpath('//input[@value="Compare branches and continue"]')
		compare_branches.click()

		submit = wait.until(EC.presence_of_element_located((By.XPATH, '//input[@value="Submit merge request"]')))
		submit.click()

		time.sleep(3)

		try:
			merge_request = wait.until(EC.presence_of_element_located((By.XPATH, '//*[contains(text(), "WIP: Branch 1")]')))
			
		except selenium.common.exceptions.TimeoutException:
			self.assertFalse(True)

		browser.quit()

	def test_08_createNewSchedule(self):
		browser = webdriver.Firefox()
		self.signIn(browser, 'https://gitlab.engr.illinois.edu/' + uname + '/TestProjectGUI/pipeline_schedules')

		wait = WebDriverWait(browser, 5)
		schedule_button = wait.until(EC.visibility_of_element_located((By.XPATH, '//span[contains(text(), "New schedule")]')))
		schedule_button.click()
		description_button = wait.until(EC.visibility_of_element_located((By.ID, 'schedule_description')))
		description_button.send_keys('This is a test schedule created as part of a GUI test')
		every_day_button = browser.find_element_by_id("every-day")
		every_day_button.click()
		submit = browser.find_element_by_xpath('//input[@class="btn btn-create"]')
		submit.click()

		try:
			element = wait.until(EC.visibility_of_element_located((By.XPATH, '//td[contains(text(), "This is a test schedule created as part of a GUI test")]')))
			self.assertTrue(True, "Schedule successfully created")

		except selenium.common.exceptions.TimeoutException:
			self.assertFalse(True, "Schedule was not successfully created")

		browser.quit()

	def test_09_createGroup(self):
		browser = webdriver.Firefox()
		self.signIn(browser, 'https://gitlab.engr.illinois.edu')

		wait = WebDriverWait(browser, 5)

		#waiting for groups button on toolbar to show 
		groups = wait.until(EC.presence_of_element_located((By.XPATH, '//a[@class="dashboard-shortcuts-groups"]')))
		groups.click()

		new_group = wait.until(EC.element_to_be_clickable((By.XPATH, '//a[@class="btn btn-new"]')))
		new_group.click()

		#locating text box on page to enter name of group
		group_path = wait.until(EC.presence_of_element_located((By.ID, 'group_path')))
		group_path.send_keys('GUITestGroup')

		create = browser.find_element_by_xpath('//input[@class="btn btn-create"]')
		create.click()
		
		time.sleep(2)
		
		#going back to groups page to see if group shows up
		browser.get('https://gitlab.engr.illinois.edu/dashboard/groups')
		
		try:
			element = wait.until(EC.presence_of_element_located((By.XPATH, '//a[contains(text(), "GUITestGroup")]')))
			self.assertTrue(True, "Created group GUITestGroup")

		except selenium.common.exceptions.TimeoutException:
			self.assertFalse(True, "Unable to create group GUITestGroup")

		browser.quit()

	def test_10_createSubgroup(self):
		browser = webdriver.Firefox()
		self.signIn(browser, 'https://gitlab.engr.illinois.edu/dashboard/groups')
		
		wait = WebDriverWait(browser, 5)
		
		element = wait.until(EC.presence_of_element_located((By.XPATH, '//a[contains(text(), "GUITestGroup")]')))
		element.click()

		button = wait.until(EC.presence_of_element_located((By.XPATH, '//button[@class="btn btn-success dropdown-toggle js-dropdown-toggle"]')))
		button.click()

		subgroup = wait.until(EC.visibility_of_element_located((By.XPATH, '//span[contains(text(), "Create a subgroup in this group.")]')))
		subgroup.click()

		create_button = wait.until(EC.visibility_of_element_located((By.XPATH, '//input[@class="btn btn-success dropdown-primary js-new-group-child"]')))
		create_button.click()

		group_path = wait.until(EC.presence_of_element_located((By.ID, 'group_path')))
		group_path.send_keys('GUITestGroupSubgroup')

		submit = browser.find_element_by_xpath('//input[@class="btn btn-create"]')
		submit.click()

		try:
			element = wait.until(EC.presence_of_element_located((By.XPATH, '//span[contains(text(), "Group \'GUITestGroupSubgroup\' was successfully created")]')))
			self.assertTrue(True, "Created subggroup GUITestGroupSubgroup")

		except selenium.common.exceptions.TimeoutException:
			self.assertFalse(True, "Unable to create subgroup GUITestGroupSubgroup")

		browser.quit()

	def test_11_createNewIssue(self):
		browser = webdriver.Firefox()
		self.signIn(browser, 'https://gitlab.engr.illinois.edu/' + uname + '/TestProjectGUI/issues')

		wait = WebDriverWait(browser, 5)
		new_issue = wait.until(EC.presence_of_element_located((By.ID, 'new_issue_link')))
		new_issue.click()
		title = wait.until(EC.presence_of_element_located((By.ID, 'issue_title')))
		title.send_keys('Test issue')
		description = browser.find_element_by_id('issue_description')
		description.send_keys("This is a test issue as part of a GUI test")
		submit = browser.find_element_by_xpath('//input[@class="btn btn-create"]')
		submit.click()

		try:
			issue_check = wait.until(EC.visibility_of_element_located((By.XPATH, '//h2[contains(text(), "Test issue")]')))
			self.assertTrue(True, "Issue was successfully created")

		except selenium.common.exceptions.TimeoutException:
			self.assertFalse(True, "Failed to create issue")

		browser.quit()

	def test_12_moveIssue(self):
		browser = webdriver.Firefox()
		self.signIn(browser, 'https://gitlab.engr.illinois.edu')
		wait = WebDriverWait(browser, 5)

		#creating a new project to move issue to
		newproj = wait.until(EC.presence_of_element_located((By.XPATH, '//*[@class="btn btn-new"]')))
		newproj.click()
		
		project_path = wait.until(EC.presence_of_element_located((By.ID, 'project_path')))
		project_path.send_keys('TestProject2GUI')
		
		submit = wait.until(EC.presence_of_element_located((By.XPATH, '//input[@class="btn btn-create project-submit"]')))
		submit.click()

		#moving issue
		dropdown = wait.until(EC.presence_of_element_located((By.XPATH, '//a[contains(text(), "Projects")]')))
		dropdown.click()
		
		dashboard = browser.find_element_by_xpath('//a[@href="/dashboard/projects"]')
		dashboard.click()
		
		testProject = wait.until(EC.presence_of_element_located((By.XPATH, '//span[contains(text(), "TestProjectGUI")]')))
		testProject.click()
		
		issues = wait.until(EC.presence_of_element_located((By.XPATH, '//span[contains(text(), "Issues")]')))
		issues.click()
		issue = wait.until(EC.visibility_of_element_located((By.XPATH, '//a[contains(text(), "Test issue")]')))
		issue.click()
		
		move_issue = wait.until(EC.visibility_of_element_located((By.XPATH, '//button[contains(text(), "Move issue")]')))
		
		browser.execute_script("window.scrollTo(0, document.body.scrollHeight)")
		
		move_issue.click()
		
		search = browser.find_element_by_id("sidebar-move-issue-dropdown-search")
		search.send_keys('TestProject2GUI')
		search.send_keys(Keys.RETURN)
		
		option = wait.until(EC.visibility_of_element_located((By.XPATH, "//a[contains(text(), '" + uname + " / TestProject2GUI')]")))
		option.click()
		
		button = wait.until(EC.element_to_be_clickable((By.XPATH, "//button[@class='btn btn-new sidebar-move-issue-confirmation-button js-move-issue-confirmation-button']")))
		time.sleep(3)
		button.click()
		
		element = wait.until(EC.visibility_of_element_located((By.XPATH, '//span[contains(text(), "TestProject2GUI")]')))
		
		browser.get('https://gitlab.engr.illinois.edu/' + uname + '/TestProject2GUI/issues')
		
		try:
			issue = wait.until(EC.visibility_of_element_located((By.XPATH, '//a[contains(text(), "Test issue")]')))
			self.assertTrue(True, "Moved issue to TestProject2GUI")

		except selenium.common.exceptions.TimeoutException:
			self.assertFalse(True, "Failed to move issue to TestProject2GUI")

		browser.quit()

	def test_13_closeIssue(self):
		browser = webdriver.Firefox()
		self.signIn(browser, 'https://gitlab.engr.illinois.edu/' + uname + '/TestProject2GUI/issues')
		wait = WebDriverWait(browser, 5)
		issue = wait.until(EC.visibility_of_element_located((By.XPATH, '//a[contains(text(), "Test issue")]')))
		issue.click()
		close_button = wait.until(EC.visibility_of_element_located((By.XPATH, '//a[@class="hidden-xs hidden-sm btn btn-grouped btn-close js-btn-issue-action "]')))
		close_button.click()

		try:
			element = wait.until(EC.visibility_of_element_located((By.XPATH, '//span[contains(text(), "Closed")]')))
			self.assertTrue(True, "Closed Issue")

		except selenium.common.exceptions.TimeoutException:
			self.assertFalse(True, "Failed to close issue")

		browser.quit()

	def test_14_delIssue(self): #############
		browser = webdriver.Firefox()
		self.signIn(browser, 'https://gitlab.engr.illinois.edu/' + uname + '/TestProject2GUI/issues')

		wait = WebDriverWait(browser, 5)
		
		closed_button = wait.until(EC.visibility_of_element_located((By.XPATH, '//span[contains(text(), "Closed")]')))
		closed_button.click()	

		issue = wait.until(EC.visibility_of_element_located((By.XPATH, '//a[contains(text(), "Test issue")]')))
		issue.click()
		
		edit_button = wait.until(EC.visibility_of_element_located((By.XPATH, '//a[@class="hidden-xs hidden-sm btn btn-grouped issuable-edit"]')))
		edit_button.click()
		
		delete_button = browser.find_element_by_xpath('//button[contains(text(), "Delete")]')
		delete_button.click()
		browser.switch_to_alert().accept()

		try:
			issue = wait.until(EC.visibility_of_element_located((By.XPATH, '//a[contains(text(), "Test issue")]')))
			self.assertFalse(True, "Did not delete Issue")

		except selenium.common.exceptions.TimeoutException:
			self.assertTrue(True, "Deleted issue")

		browser.quit()

	def test_15_starProject(self):
		browser = webdriver.Firefox()
		self.signIn(browser, 'https://gitlab.engr.illinois.edu/' + uname + '/TestProjectGUI')
		wait = WebDriverWait(browser, 5)
		star = wait.until(EC.visibility_of_element_located((By.XPATH, '//span[contains(text(), "Star")]')))
		star.click()

		dropdown = browser.find_element_by_xpath('//a[contains(text(), "Projects")]')
		dropdown.click()
		starred = browser.find_element_by_xpath('//a[@href="/dashboard/projects/starred"]')
		starred.click()

		try:
			issue = wait.until(EC.visibility_of_element_located((By.XPATH, '//span[contains(text(), "TestProjectGUI")]')))
			self.assertTrue(True, "Starred project 'TestProjectGUI'")

		except selenium.common.exceptions.TimeoutException:
			self.assertFalse(True, "Failed to star project 'TestProjectGUI'")

		browser.quit()

	def test_16_createIssueFromBoard(self):
		browser = webdriver.Firefox()
		self.signIn(browser, 'https://gitlab.engr.illinois.edu/' + uname + '/TestProjectGUI')

		wait = WebDriverWait(browser, 5)
		
		issues = wait.until(EC.presence_of_element_located((By.XPATH, '//span[contains(text(), "Issues")]')))
		issues.click()

		board = wait.until(EC.presence_of_element_located((By.XPATH, '//a[@title="Board"]')))
		board.click()

		element_to_wait = wait.until(EC.presence_of_element_located((By.XPATH, '//span[@class="board-title-text has-tooltip"]')))
		buttons = browser.find_elements_by_xpath('//button[@class="issue-count-badge-add-button btn btn-sm btn-default has-tooltip js-no-trigger-collapse"]')
		buttons[0].click()
		
		form = wait.until(EC.visibility_of_element_located((By.XPATH, "//input[contains(@id, '-title')]"))) 
		form.send_keys('issue1')
		
		submit = wait.until(EC.element_to_be_clickable((By.XPATH, '//button[contains(text(), "Submit issue")]')))
		submit.click()
		
		time.sleep(3)
		
		browser.get('https://gitlab.engr.illinois.edu/' + uname + '/TestProjectGUI/issues')

		try:
			element = wait.until(EC.presence_of_element_located((By.XPATH, '//a[contains(text(), "issue1")]')))
			self.assertTrue(True, "Created issue from board successfully")

		except selenium.common.exceptions.TimeoutException:
			self.assertFalse(True, "Unable to create issue from board")

		browser.quit()

	def test_17_disableWikiSettings(self):   #########
		browser = webdriver.Firefox()
		self.signIn(browser, 'https://gitlab.engr.illinois.edu/' + uname + '/TestProjectGUI')
		
		wait = WebDriverWait(browser, 8)
		
		settings = wait.until(EC.visibility_of_element_located((By.XPATH, '//span[contains(text(), "Settings")]')))
		settings.click()

		title = wait.until(EC.visibility_of_element_located((By.XPATH, '//*[contains(text(), "General project settings")]')))	
		buttons = browser.find_elements_by_xpath('//button[@class="btn js-settings-toggle"]')
		buttons[1].click()
		browser.execute_script("window.scrollTo(0, document.body.scrollHeight)")
		
		buttons = browser.find_elements_by_xpath('//button[@class="project-feature-toggle checked"]')
		buttons[4].send_keys(Keys.RETURN)

		save = browser.find_elements_by_xpath('//input[@value="Save changes"]')
		save[1].click()

		print(len(save))

		time.sleep(2)
		browser.get('https://gitlab.engr.illinois.edu/' + uname + '/TestProjectGUI/')

		try:
			wiki_button = wait.until(EC.visibility_of_element_located((By.XPATH, '//span[contains(text(), "Wiki")]')))
			self.assertFalse(True, "Did not disable wiki settings")

		except selenium.common.exceptions.TimeoutException:
			self.assertTrue(True, "Disabled wiki settings successfully")

		browser.quit()

	def test_18_checkActivity(self):
		browser = webdriver.Firefox()

		self.signIn(browser, 'https://gitlab.engr.illinois.edu/' + uname + '/TestProjectGUI/activity')
		
		try:
			wait = WebDriverWait(browser, 5)
			element = wait.until(EC.visibility_of_element_located((By.XPATH, '//div[contains(text(), "issue1")]')))
			self.assertTrue(True, "Activity shows issue1 has been created in a previous test")

		except selenium.common.exceptions.TimeoutException:
			self.assertFalse(True, "Activity does not show issue1 has been created in a previous test")

		browser.quit()

	def test_19_createSnippet(self):
		browser = webdriver.Firefox()
		self.signIn(browser, 'https://gitlab.engr.illinois.edu/' + uname + '/TestProjectGUI')
		wait = WebDriverWait(browser, 5)
		snippet = wait.until(EC.presence_of_element_located((By.XPATH, '//span[contains(text(), "Snippets")]')))
		snippet.click()
		
		new_snippet = wait.until(EC.visibility_of_element_located((By.XPATH, '//a[@class="btn btn-new"]')))
		new_snippet.click()
		
		title = wait.until(EC.presence_of_element_located((By.ID, 'project_snippet_title')))
		title.send_keys('TestProjectGUI Snippet')
		description = browser.find_element_by_id('project_snippet_description')
		description.send_keys('This is a test snippet created as part of a GUI test')
		file = browser.find_element_by_class_name('ace_text-input')
		file.send_keys('snippet.py')
		
		submit = browser.find_element_by_xpath('//input[@class="btn-create btn"]')
		submit.click()
		snippet = wait.until(EC.presence_of_element_located((By.XPATH, '//span[contains(text(), "Snippets")]')))
		snippet.click()

		try:
			element = wait.until(EC.presence_of_element_located((By.XPATH, '//a[contains(text(), "TestProjectGUI Snippet")]')))
			self.assertTrue(True, "Created snippet for TestProjectGUI")

		except selenium.common.exceptions.TimeoutException:
			self.assertFalse(True, "Could not create snippet for TestProjectGUI")

		browser.quit()

	def test_20_delSnippet(self):
		browser = webdriver.Firefox()
		self.signIn(browser, 'https://gitlab.engr.illinois.edu/' + uname + '/TestProjectGUI/snippets')
		wait = WebDriverWait(browser, 5)
		element = wait.until(EC.presence_of_element_located((By.XPATH, '//a[contains(text(), "TestProjectGUI Snippet")]')))
		element.click()
		delete = wait.until(EC.presence_of_element_located((By.XPATH, '//a[contains(text(), "Delete")]')))
		delete.click()
		browser.switch_to_alert().accept()

		try:
			element = wait.until(EC.presence_of_element_located((By.XPATH, '//a[contains(text(), "TestProjectGUI Snippet")]')))
			self.assertFalse(True, "TestProjectGUI Snippet was not deleted")

		except selenium.common.exceptions.TimeoutException:
			self.assertTrue(True, "Deleted TestProjectGUI Snippet successfully")

		browser.quit()

	def test_21_createProjMilestone(self):
		browser = webdriver.Firefox()
		self.signIn(browser, 'https://gitlab.engr.illinois.edu/' + uname + '/TestProjectGUI')

		wait = WebDriverWait(browser, 5)

		repo_button = wait.until(EC.presence_of_element_located((By.XPATH, '//span[contains(text(), "Repository")]')))
		repo_button.click()

		time.sleep(1)

		milestone = browser.find_element_by_class_name('dashboard-shortcuts-milestones')
		milestone.click()

		new_milestone = wait.until(EC.visibility_of_element_located((By.XPATH, '//a[@class="btn btn-new new-project-item-link"]')))
		new_milestone.click()

		select = browser.find_element_by_id('s2id_autogen1_search')
		select.send_keys('TestProjectGUI')
		time.sleep(1)
		select.send_keys(Keys.RETURN)

		new_milestone.click()
		
		title = wait.until(EC.visibility_of_element_located((By.ID, 'milestone_title')))
		title.send_keys('TestProjectGUI Milestone')
		
		description = browser.find_element_by_id('milestone_description')
		description.send_keys('This is a milestone created for TestProjectGUI via a GUI test')
		
		submit = wait.until(EC.element_to_be_clickable((By.XPATH, '//input[@class="btn-create btn"]')))
		submit.click()

		try:
			final_check = wait.until(EC.visibility_of_element_located((By.XPATH, '//span[contains(text(), "Assign some issues to this milestone.")]')))
			self.assertTrue(True, "Milestone for TestProjectGUI successfully created")

		except selenium.common.exceptions.TimeoutException:
			self.assertFalse(True, "Milestone was not created")

		browser.quit()

	def test_22_delMilestone(self):
		browser = webdriver.Firefox()
		self.signIn(browser, 'https://gitlab.engr.illinois.edu')

		wait = WebDriverWait(browser, 5)
		milestone = wait.until(EC.visibility_of_element_located((By.XPATH, '//a[@class="dashboard-shortcuts-milestones"]')))
		milestone.click()

		link = wait.until(EC.visibility_of_element_located((By.XPATH, '//a[@href="/dashboard/milestones/testprojectgui-milestone?title=TestProjectGUI+Milestone"]')))
		link.click()
		testproject_milestone = wait.until(EC.visibility_of_element_located((By.XPATH, "//a[contains(text(), '" + uname + " / TestProjectGUI')]")))
		testproject_milestone.click()

		delete_button = wait.until(EC.visibility_of_element_located((By.XPATH, '//a[contains(text(), "Delete")]')))
		delete_button.click()
		time.sleep(1)
		browser.switch_to_alert().accept()
		
		#need to re declare it since it is not attached to the DOM anymore
		milestone = wait.until(EC.visibility_of_element_located((By.XPATH, '//a[@class="dashboard-shortcuts-milestones"]')))
		milestone.click()

		try:
			link = wait.until(EC.visibility_of_element_located((By.XPATH, '//a[@href="/dashboard/milestones/a?title=TestProjectGUI+Milestone"]')))
			self.assertFalse(True, "Milestone was not deleted")

		except selenium.common.exceptions.TimeoutException:
			self.assertTrue(True, "Milestone was deleted properly")	

		browser.quit()	

	def test_23_createPage(self):
		browser = webdriver.Firefox()
		self.signIn(browser, 'https://gitlab.engr.illinois.edu/' + uname + '/TestProjectGUI')
		
		wait = WebDriverWait(browser, 5)
		settings = wait.until(EC.visibility_of_element_located((By.XPATH, '//span[contains(text(), "Settings")]')))
		settings.click()

		title = wait.until(EC.visibility_of_element_located((By.XPATH, '//*[contains(text(), "General project settings")]')))	
		buttons = browser.find_elements_by_xpath('//button[@class="btn js-settings-toggle"]')
		buttons[1].click()
		browser.execute_script("window.scrollTo(0, document.body.scrollHeight)")
		
		button = browser.find_element_by_xpath('//button[@class="project-feature-toggle"]')
		button.send_keys(Keys.RETURN)

		save = browser.find_elements_by_xpath('//input[@value="Save changes"]')
		save[1].click()
		time.sleep(2)
		browser.get('https://gitlab.engr.illinois.edu/' + uname + '/TestProjectGUI/')


		wiki_button = wait.until(EC.visibility_of_element_located((By.XPATH, '//span[contains(text(), "Wiki")]')))
		wiki_button.click()	
		element = wait.until(EC.visibility_of_element_located((By.ID, "wiki_content")))
		element.send_keys("some example content")
		submit = browser.find_element_by_xpath('//input[@class="btn-create btn"]')
		submit.click()
			
		try:
			page = wait.until(EC.visibility_of_element_located((By.XPATH, '//h2[contains(text(), "Home")]')))
			self.assertTrue(True, "Page successfully created")
		except selenium.common.exceptions.TimeoutException:
			self.assertFalse(True, "Page was not successfully created")

		browser.quit()

	def test_24_editPageTitle(self):
		browser = webdriver.Firefox()
		self.signIn(browser, 'https://gitlab.engr.illinois.edu/' + uname + '/TestProjectGUI')
		
		wait = WebDriverWait(browser, 5)
		wiki_button = wait.until(EC.visibility_of_element_located((By.XPATH, '//span[contains(text(), "Wiki")]')))
		wiki_button.click()
		
		edit = wait.until(EC.visibility_of_element_located((By.XPATH, '//a[@class="btn js-wiki-edit"]')))
		edit.click()

		history = wait.until(EC.visibility_of_element_located((By.XPATH, '//a[contains(text(), "Page history")]')))
		history.click()

		try:
			element = wait.until(EC.presence_of_element_located((By.XPATH, "//td[contains(text(), '" + uname + " created page: home')]")))
			self.assertTrue(True, "Page history contains most recent history")
		
		except selenium.common.exceptions.TimeoutException:
			self.assertFalse(True, "Test failed")

		browser.quit()

	def test_25_delPage(self):
		browser = webdriver.Firefox()
		self.signIn(browser, 'https://gitlab.engr.illinois.edu/' + uname + '/TestProjectGUI/wikis/home')

		wait = WebDriverWait(browser, 5)
		
		edit = wait.until(EC.visibility_of_element_located((By.XPATH, '//a[@class="btn js-wiki-edit"]')))
		edit.click()

		delete = wait.until(EC.visibility_of_element_located((By.XPATH, '//a[contains(text(), "Delete")]')))
		delete.click()
		time.sleep(2)
		browser.switch_to_alert().accept()
	
		try:
			page = wait.until(EC.visibility_of_element_located((By.XPATH, '//span[contains(text(), "Page was successfully deleted")]')))
			self.assertTrue(True, "Page was successfully deleted")
		
		except selenium.common.exceptions.TimeoutException:
			self.assertTrue(True, "Page was not deleted")

		browser.quit()

	def test_26_checkCommitHistory(self):
		browser = webdriver.Firefox()

		self.signIn(browser, 'https://gitlab.engr.illinois.edu/' + uname + '/TestProjectGUI/commits/master')

		wait = WebDriverWait(browser, 5)
	
		try:
			message = wait.until(EC.visibility_of_element_located((By.XPATH, '//a[contains(text(), "Added readme.md")]')))
			message2 = browser.find_element_by_xpath('//a[contains(text(), "Add new file")]')

			self.assertTrue(True, "Commit history reflects add")
		
		except selenium.common.exceptions.TimeoutException:
			self.assertTrue(True, "Test failed")

		browser.quit()

	def test_27_renameRepo(self):
		browser = webdriver.Firefox()
		self.signIn(browser, 'https://gitlab.engr.illinois.edu/' + uname + '/TestProject2GUI/')
		wait = WebDriverWait(browser, 5)
		
		settings = wait.until(EC.visibility_of_element_located((By.XPATH, '//span[contains(text(), "Settings")]')))
		settings.click()
		
		title = wait.until(EC.visibility_of_element_located((By.XPATH, '//*[contains(text(), "General project settings")]')))	
		
		buttons = browser.find_elements_by_xpath('//button[@class="btn js-settings-toggle"]')
		buttons[4].click()
		
		browser.execute_script("window.scrollTo(0, document.body.scrollHeight)")
		title = wait.until(EC.visibility_of_element_located((By.XPATH, '//input[@id="project_name"]')))
		title.clear()
		title.send_keys('TestProject2GUIRename')
		
		form = browser.find_element_by_xpath('//input[@id="project_path"]')
		form.clear()
		form.send_keys('TestProject2GUIRename')
		
		rename_button = browser.find_element_by_xpath('//input[@value="Rename project"]')
		rename_button.click()

		browser.get('https://gitlab.engr.illinois.edu/dashboard/projects')

		try:
			element = wait.until(EC.visibility_of_element_located((By.XPATH, '//span[contains(text(), "TestProject2GUIRename")]')))	
			self.assertTrue(True, "Renamed TestProject2GUI to TestProject2GUIRename")
		
		except:
			self.assertFalse(True, "Failed to rename TestProject2GUI to TestProject2GUIRename")

		browser.quit()

	def test_28_logout(self):
		browser = webdriver.Firefox()
		self.signIn(browser, 'https://gitlab.engr.illinois.edu')
		wait = WebDriverWait(browser, 5)
		logout_avatar = wait.until(EC.visibility_of_element_located((By.XPATH, '//img[@class="header-user-avatar js-lazy-loaded"]')))
		logout_avatar.click()
		sign_out_link = wait.until(EC.visibility_of_element_located((By.XPATH, '//a[@class="sign-out-link"]')))
		sign_out_link.click()

		try:
			element = wait.until(EC.visibility_of_element_located((By.XPATH, '//input[@id="username"]')))
			self.assertTrue(True, "Logged out successfully")

		except selenium.common.exceptions.TimeoutException:
			self.assertFalse(True, "Was not able to logout successfully")

		browser.quit()

	def test_29_delSchedule(self):
		browser = webdriver.Firefox() 
		self.signIn(browser, 'https://gitlab.engr.illinois.edu/' + uname + '/TestProjectGUI/pipeline_schedules')

		wait = WebDriverWait(browser, 5)
		
		#waiting for a specific element on the page to appear before proceeding
		element = wait.until(EC.visibility_of_element_located((By.XPATH, '//a[@class="btn btn-remove"]')))
		
		#all 'trash can' buttons on page
		buttons = browser.find_elements_by_xpath('//a[@class="btn btn-remove"]')  

		#since we are only creating one schedule
		button = buttons[0]
		button.click()

		#accept browser alert
		browser.switch_to_alert().accept()

		time.sleep(2)

		try:
			#if this element appears on the page that means the schedule was not deleted
			element = wait.until(EC.visibility_of_element_located((By.XPATH, '//td[contains(text(), "This is a test schedule created as part of a GUI test")]')))
			self.assertFalse(True, "Schedule was not deleted")
		
		except selenium.common.exceptions.TimeoutException:
			#if timeout occurs it means the element is not on page
			self.assertTrue(True, "Deleted schedule successfully")

		browser.quit()

	def test_30_checkContributors(self):
		browser = webdriver.Firefox()
		self.signIn(browser, 'https://gitlab.engr.illinois.edu/' + uname + '/TestProjectGUI')

		wait = WebDriverWait(browser, 5)
		
		#click on settings button on side
		settings = wait.until(EC.visibility_of_element_located((By.XPATH, '//span[contains(text(), "Settings")]')))
		settings.click()

		#clicking on members button
		members_button = wait.until(EC.visibility_of_element_located((By.XPATH, '//span[contains(text(), "Members")]')))
		members_button.click()

		try:
			#checking to see if your username shows up in contributors page
			username = wait.until(EC.presence_of_element_located((By.XPATH, "//a[@href='/" + uname + "']")))
			self.assertTrue(True, "Test passed")
		
		except selenium.common.exceptions.TimeoutException:
			self.assertFalse(True, "Test failed -- your username is not showing up on page")

		browser.quit()

	def test_31_delBranch(self):
		browser = webdriver.Firefox()

		self.signIn(browser, 'https://gitlab.engr.illinois.edu/' + uname + '/TestProjectGUI')

		wait = WebDriverWait(browser, 5)
		
		repo_button = wait.until(EC.presence_of_element_located((By.XPATH, '//span[contains(text(), "Repository")]')))
		repo_button.click()

		branches = branches = wait.until(EC.visibility_of_element_located((By.XPATH, '//a[contains(text(), "Branches")]')))
		branches.click()

		element_to_wait = wait.until(EC.visibility_of_element_located((By.XPATH, '//a[@class="btn btn-remove remove-row js-ajax-loading-spinner has-tooltip"]')))
		del_button = browser.find_elements_by_xpath('//a[@class="btn btn-remove remove-row js-ajax-loading-spinner has-tooltip"]')

		button = del_button[0]
		button.click()
		browser.switch_to_alert().accept()

		try:
			message = wait.until(EC.visibility_of_element_located((By.XPATH, '//a[@href=/"' + uname + "'/TestProjectGUI/tree/branch-1']")))
			self.assertFalse(True, "Branch was not deleted")
		
		except selenium.common.exceptions.TimeoutException:
			self.assertTrue(True, "Deleted branch successfully")

		browser.quit()

	def test_32_createTag(self):
		browser = webdriver.Firefox()

		self.signIn(browser, 'https://gitlab.engr.illinois.edu/' + uname + '/TestProjectGUI')

		wait = WebDriverWait(browser, 5)
		
		repo_button = wait.until(EC.presence_of_element_located((By.XPATH, '//span[contains(text(), "Repository")]')))
		repo_button.click()

		tags_button = wait.until(EC.presence_of_element_located((By.XPATH, '//a[contains(text(), "Tags")]')))
		tags_button.click()

		new_tag = wait.until(EC.presence_of_element_located((By.XPATH, '//a[contains(text(), "New tag")]')))
		new_tag.click()

		tagname = wait.until(EC.presence_of_element_located((By.ID, 'tag_name')))
		tagname.send_keys('TestProjectGUITag')

		message = browser.find_element_by_xpath('//textarea[@id="message"]')
		message.send_keys('this is part of a GUI test')

		create_button = wait.until(EC.element_to_be_clickable((By.XPATH, '//button[contains(text(), "Create tag")]')))
		create_button.click()

		time.sleep(2)

		browser.get('https://gitlab.engr.illinois.edu/' + uname + '/TestProjectGUI/tags')

		try:
			message = wait.until(EC.visibility_of_element_located((By.XPATH, '//a[contains(text(), "TestProjectGUITag")]')))
			self.assertTrue(True, "Tag created successfully")
		
		except selenium.common.exceptions.TimeoutException:
			self.assertFalse(True, "Tag was not created")

		browser.quit()

	def test_33_delTag(self):
		browser = webdriver.Firefox()
		self.signIn(browser, 'https://gitlab.engr.illinois.edu/' + uname + '/TestProjectGUI')

		wait = WebDriverWait(browser, 5)
		
		#locating and clicking repository button on side panel
		repo_button = wait.until(EC.presence_of_element_located((By.XPATH, '//span[contains(text(), "Repository")]')))
		repo_button.click()

		#locating and clicking tags button
		tags_button = wait.until(EC.presence_of_element_located((By.XPATH, '//a[contains(text(), "Tags")]')))
		tags_button.click()

		#deleting the tag we created in the previous test
		del_button = wait.until(EC.presence_of_element_located((By.XPATH, '//a[@class="btn btn-remove remove-row has-tooltip "]')))
		buttons = browser.find_elements_by_xpath('//a[@class="btn btn-remove remove-row has-tooltip "]')
		button = buttons[0]

		button.click()

		#accepting the browser alert
		browser.switch_to_alert().accept()	

		try:
			#the deleted tag should now show up on the page
			element = wait.until(EC.visibility_of_element_located((By.XPATH, '//span[contains(text(), "TestProjectGUITag")]')))
			self.assertFalse(True, "Tag was not deleted")
		
		#if there is a timeout it means we deleted it successfully
		except selenium.common.exceptions.TimeoutException:
			self.assertTrue(True, "Tag deleted successfully")

		browser.quit()

	def test_34_delGroup(self):
		browser = webdriver.Firefox()
		self.signIn(browser, 'https://gitlab.engr.illinois.edu/' + uname + '/TestProjectGUI')

		wait = WebDriverWait(browser, 5)
		
		#waiting for groups button to show up on page
		groups = wait.until(EC.presence_of_element_located((By.XPATH, '//a[@class="dashboard-shortcuts-groups"]')))
		groups.click()

		#going to the edits page of the group
		browser.get('https://gitlab.engr.illinois.edu/groups/GUITestGroup/-/edit')

		#waiting for remove group button to show and clicking it after
		remove_group = wait.until(EC.element_to_be_clickable((By.XPATH, '//input[@value="Remove group"]')))
		remove_group.click()

		#sending name of group to confirmation box 
		confirm_box = wait.until(EC.presence_of_element_located((By.ID, 'confirm_name_input')))
		confirm_box.send_keys('GUITestGroup')

		#confirming deletion 
		confirm_button = wait.until(EC.element_to_be_clickable((By.XPATH, '//input[@value="Confirm"]')))
		confirm_button.click()

		#going back to groups page to check for element
		groups = wait.until(EC.presence_of_element_located((By.XPATH, '//a[@class="dashboard-shortcuts-groups"]')))
		groups.click()

		try:
			#if element shows up on page that means we did not delete it 
			element = wait.until(EC.presence_of_element_located((By.XPATH, '//a[contains(text(), "GUITestGroup")]')))
			self.assertFalse(True, "Group deleted successfully")
		
		except selenium.common.exceptions.TimeoutException:
			self.assertTrue(True, "Group was not deleted")

		browser.quit()

	def test_35_runHousekeeping(self):
		browser = webdriver.Firefox()
		self.signIn(browser, 'https://gitlab.engr.illinois.edu/' + uname + '/TestProjectGUI')

		wait = WebDriverWait(browser, 5)
		
		#click on settings button on side
		settings = wait.until(EC.visibility_of_element_located((By.XPATH, '//span[contains(text(), "Settings")]')))
		settings.click()

		#waiting for a specific element on page to be loaded before proceeding
		title = wait.until(EC.visibility_of_element_located((By.XPATH, '//*[contains(text(), "General project settings")]')))	
		
		#multiple toggle buttons on page -- we want the last one
		buttons = browser.find_elements_by_xpath('//button[@class="btn js-settings-toggle"]')
		buttons[4].click()
		
		#scrolling to bottom of page
		browser.execute_script("window.scrollTo(0, document.body.scrollHeight)")

		housekeeping_button = wait.until(EC.visibility_of_element_located((By.XPATH, '//a[contains(text(), "Run housekeeping")]')))
		housekeeping_button.click()

		try:
			#specific message will show up after we click on 'Run housekeeping' button -- we are checking for that message on the page
			element = wait.until(EC.presence_of_element_located((By.XPATH, '//span[contains(text(), "Housekeeping successfully started")]')))
			self.assertTrue(True, "Started housekeeping for repo")
		
		except selenium.common.exceptions.TimeoutException:
			self.assertFalse(True, "Test failed -- did not start housekeeping")

		browser.quit()


	def test_36_addLicense(self):
		browser = webdriver.Firefox()
		self.signIn(browser, 'https://gitlab.engr.illinois.edu/' + uname + '/TestProjectGUI')
		wait = WebDriverWait(browser, 5)
		
		#waiting for the add license button to show up on page
		license_button = wait.until(EC.visibility_of_element_located((By.XPATH, '//a[contains(text(), "Add License")]')))
		license_button.click()

		#locating the text box to add information about the license
		text = wait.until(EC.visibility_of_element_located((By.XPATH, '//textarea[@class="ace_text-input"]')))
		
		#sending some text to this box
		text.send_keys('some license')

		#locating and clicking on commit button
		commit_button = browser.find_element_by_xpath('//button[contains(text(), "Commit changes")]')
		commit_button.click()

		try:
			#waiting for file name to show up on page -- if it shows up before timeout it means it was created 
			message = wait.until(EC.visibility_of_element_located((By.XPATH, '//span[contains(text(), "The file has been successfully created")]')))
			self.assertTrue(True, "License added successfully")
		
		#if timeout occurs it means the test failed
		except selenium.common.exceptions.TimeoutException:
			self.assertFalse(True, "License was not added")

		browser.quit()

	def test_37_checkFiles(self):
		browser = webdriver.Firefox()
		self.signIn(browser, 'https://gitlab.engr.illinois.edu/' + uname + '/TestProjectGUI/tree/master')

		wait = WebDriverWait(browser, 5)
	
		try:
			license = wait.until(EC.visibility_of_element_located((By.XPATH, '//span[contains(text(), "LICENSE")]')))
			readme = wait.until(EC.visibility_of_element_located((By.XPATH, '//span[contains(text(), "README.md")]')))

			self.assertTrue(True, "Able to view files from url")

		except selenium.common.exceptions.TimeoutException:
			self.assertFalse(True, "Test failed")
		
		browser.quit()

	def test_38_setProjDescription(self):
		browser = webdriver.Firefox()
		self.signIn(browser, 'https://gitlab.engr.illinois.edu/' + uname + '/TestProjectGUI')

		wait = WebDriverWait(browser, 5)
		
		#click on settings button on side
		settings = wait.until(EC.visibility_of_element_located((By.XPATH, '//span[contains(text(), "Settings")]')))
		settings.click()

		title = wait.until(EC.visibility_of_element_located((By.XPATH, '//*[contains(text(), "General project settings")]')))	
		
		#many buttons on page with same attributes -- so we use the function find_elements_by_xpath instead of find_element_by_xpath
		buttons = browser.find_elements_by_xpath('//button[@class="btn js-settings-toggle"]')
		
		#button we want is the first one
		general_settings_button = buttons[0].click()
		
		description = wait.until(EC.visibility_of_element_located((By.ID, 'project_description')))	
		
		#clearing out textbox in case something is already present 
		description.clear()
		description.send_keys('This is the repo specifically meant for executing GUI tests')

		#multiple save buttons on page with same attributes -- choosing first one
		save_buttons = browser.find_elements_by_xpath('//input[@value="Save changes"]')
		save_changes = save_buttons[0]
		save_changes.click()
		
		time.sleep(2)

		#description shows up on homepage of repository
		browser.get('https://gitlab.engr.illinois.edu/' + uname + '/TestProjectGUI')

		try:
			element = wait.until(EC.visibility_of_element_located((By.XPATH, '//*[contains(text(), "This is the repo specifically meant for executing GUI tests")]')))
			self.assertTrue(True, "Updated description")
		
		except selenium.common.exceptions.TimeoutException:
			self.assertTrue(True, "Did not update description")

		browser.quit()

	def test_39_archiveProject(self):
		browser = webdriver.Firefox()
		self.signIn(browser, 'https://gitlab.engr.illinois.edu/' + uname + '/TestProjectGUI')

		wait = WebDriverWait(browser, 5)
		
		#going to settings page
		settings = wait.until(EC.visibility_of_element_located((By.XPATH, '//span[contains(text(), "Settings")]')))
		settings.click()

		title = wait.until(EC.visibility_of_element_located((By.XPATH, '//*[contains(text(), "Advanced settings")]')))	

		#there are five buttons on page with same class -- we want the last one
		buttons = browser.find_elements_by_xpath('//button[@class="btn js-settings-toggle"]')
		buttons[4].click()

		#scrolling to the bottom of the page
		browser.execute_script("window.scrollTo(0, document.body.scrollHeight)")

		#archiving project
		archive = wait.until(EC.visibility_of_element_located((By.XPATH, '//a[contains(text(), "Archive project")]')))
		archive.click()

		#accepting browser alert
		browser.switch_to_alert().accept()

		time.sleep(3)

		browser.get('https://gitlab.engr.illinois.edu/dashboard/projects')

		try:
			#an archived project should not show up in the dashboard page
			element = wait.until(EC.visibility_of_element_located((By.XPATH, '//span[contains(text(), "TestProjectGUI")]')))
			self.assertFalse(True, "Did not archive project")
		
		except selenium.common.exceptions.TimeoutException:
			self.assertTrue(True, "Archived project successfully")

		browser.quit()

	def test_40_delProj(self):
		browser = webdriver.Firefox()
		self.signIn(browser, 'https://gitlab.engr.illinois.edu/' + uname + '/TestProjectGUI')

		wait = WebDriverWait(browser, 5)
		
		#locating and clicking on settings button
		settings = wait.until(EC.visibility_of_element_located((By.XPATH, '//span[contains(text(), "Settings")]')))
		settings.click()
		
		#waiting for a specific element on page to be loaded before proceeding
		title = wait.until(EC.visibility_of_element_located((By.XPATH, '//*[contains(text(), "General project settings")]')))	
		
		#multiple toggle buttons on page -- we want the last one
		buttons = browser.find_elements_by_xpath('//button[@class="btn js-settings-toggle"]')
		buttons[4].click()
		
		#scrolling to bottom of page
		browser.execute_script("window.scrollTo(0, document.body.scrollHeight)")

		#waiting for remove project button to be interactable
		rename_button = wait.until(EC.element_to_be_clickable((By.XPATH, '//input[@value="Remove project"]')))
		rename_button.click()

		#entering project name to confirm deletion
		confirmation_bar = wait.until(EC.presence_of_element_located((By.ID, 'confirm_name_input')))
		confirmation_bar.send_keys('TestProjectGUI')

		#clicking on confirm button
		confirm_button = wait.until(EC.element_to_be_clickable((By.XPATH, '//input[@value="Confirm"]')))
		confirm_button.click()

		time.sleep(3)

		#trying to locate to repo URL
		browser.get('https://gitlab.engr.illinois.edu/' + uname + '/TestProjectGUI')

		try:
			#if there is a 404 error it means the repo was deleted
			element = wait.until(EC.visibility_of_element_located((By.XPATH, "//h1[contains(text(), '404')]")))
			self.assertTrue(True, "Repo deleted successfully")
		
		except selenium.common.exceptions.TimeoutException:
			self.assertFalse(True, "Repo was not deleted")

		browser.quit()

	def test_40b_delProj2(self):
		browser = webdriver.Firefox()
		self.signIn(browser, 'https://gitlab.engr.illinois.edu/' + uname + '/TestProject2GUIRename')

		wait = WebDriverWait(browser, 5)
		
		#locating and clicking on settings button
		settings = wait.until(EC.visibility_of_element_located((By.XPATH, '//span[contains(text(), "Settings")]')))
		settings.click()
		
		#waiting for a specific element on page to be loaded before proceeding
		title = wait.until(EC.visibility_of_element_located((By.XPATH, '//*[contains(text(), "General project settings")]')))	
		
		#multiple toggle buttons on page -- we want the last one
		buttons = browser.find_elements_by_xpath('//button[@class="btn js-settings-toggle"]')
		buttons[4].click()
		
		#scrolling to bottom of page
		browser.execute_script("window.scrollTo(0, document.body.scrollHeight)")

		#waiting for remove project button to be interactable
		rename_button = wait.until(EC.element_to_be_clickable((By.XPATH, '//input[@value="Remove project"]')))
		rename_button.click()

		#entering project name to confirm deletion
		confirmation_bar = wait.until(EC.presence_of_element_located((By.ID, 'confirm_name_input')))
		confirmation_bar.send_keys('TestProject2GUIRename')

		#clicking on confirm button
		confirm_button = wait.until(EC.element_to_be_clickable((By.XPATH, '//input[@value="Confirm"]')))
		confirm_button.click()

		time.sleep(3)

		#trying to locate to repo URL
		browser.get('https://gitlab.engr.illinois.edu/' + uname + '/TestProjectGUI')

		try:
			#if there is a 404 error it means the repo was deleted
			element = wait.until(EC.visibility_of_element_located((By.XPATH, "//h1[contains(text(), '404')]")))
			self.assertTrue(True, "Repo deleted successfully")
		
		except selenium.common.exceptions.TimeoutException:
			self.assertFalse(True, "Repo was not deleted")

		browser.quit()


if __name__ == '__main__':
	unittest.main()
