This repository contains code for Selenium tests for the Gitlab application. There are 40 tests that were written, and you can run them as well if you 
setup the following tools/software:

1. geckodriver 0.16.0 
2. Mozilla Firefox 57.0.1
3. Python version 3.6
4. pip3.6
5. selenium 3.4.0
    - can do this by running: pip3.6 install selenium==3.4.0
    
After installing these tools you will be able to run the tests. Also, these tests require you to have an account with gitlab.engr.illinois.edu (UIUC net ID), 
since all the tests are run on UIUC Gitlab. You do not need to have any previous items or specific requirements on your Gitlab account, as the tests will 
create a new repository and run all the tests on that. Please make sure you have enough space on your account to create a new repository, as the school 
places limits on the amount of projects you can create. 