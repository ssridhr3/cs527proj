import subprocess 
import sys
import git
import argparse
import os
from os import listdir
from os.path import isfile, join

def get_all_commit_messages(repo, repo_path):
	os.chdir(repo_path)
	process = subprocess.Popen('git rev-list --count master', shell=True, stdout=subprocess.PIPE)
	num_messages = process.communicate()[0]
	num_messages = int(num_messages.strip())

	commits = list(repo.iter_commits('master', max_count=num_messages))
	messages = []
	for commit in commits:
		messages.append(commit.message)

	return messages 

def run_tests(repo_tests_path):
	mypath= "AddOn-Tests/tests/desktop"
	tests = [f for f in listdir(mypath) if isfile(join(mypath, f))]
	
	print(tests)


if __name__ == '__main__':
	parser = argparse.ArgumentParser()
	parser.add_argument('--repo', help="The path to the git repo", required=True)
	parser.add_argument('--run_command', help="The command used to run the tests in the repo", required=True)
	parser.add_argument('--tests_path', help="The path to the git repo's tests to be run", required=True)

	inputs = sys.argv
	repo_path = ""
	run = ""
	repo_tests_path = ""

	if('--repo' not in inputs):
		raise Exception("Need to include a git repo path\n")

	if('--run_command' not in inputs):
		raise Exception("Need to include the command to run the tests\n")

	if('--tests_path' not in inputs):
		raise Exception("Need to include the path to the repo tests\n")

	i = 0
	for inp in inputs:
		if(inp == '--repo'):
			repo_path = inputs[i+1]

		elif(inp == '--run_command'):
			run = inputs[i+1]

		elif(inp == '--tests_path'):
			repo_tests_path = inputs[i+1]

		i += 1

	repo = git.Repo(repo_path)
	run_tests(repo_tests_path)
	commit_messages = get_all_commit_messages(repo, repo_path)


	

